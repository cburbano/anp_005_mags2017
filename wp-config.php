<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nuevo_mags');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zAw+hsn6vEHEG`Io)-QeQq?1J+nLmLecinmyXR*<y=z)I( fN6JL]]>=C$[_uVuT');
define('SECURE_AUTH_KEY',  'sj>H0k7bFn_`zj>us0?0pxsn2~F!#={f4(LY[c_5B4T&{P^~:zp6Cf(lHXH)JGmR');
define('LOGGED_IN_KEY',    '&g!@{nD?JVU$8rD&H4]t^jIK_K`6~f8Z$->Rj?ZKdPb,.luu^Y_xSa{xkjmWUjr2');
define('NONCE_KEY',        'tnJC27yJ}$TCQo~lMH,newa%LIyEHLft=%wvIFKJrN#VHw(HiP3SMu!KT ch!o!i');
define('AUTH_SALT',        ']45^f|=0I0&jF0Dm4cNZhZ#Y1b|rG|d3Y;>|+ kNm!J/*:e&Ml4 1jFqSOk8bsVj');
define('SECURE_AUTH_SALT', 'Z}{i]tUb^gt_(6{yndd#TjjMo_;Pk%ja5ls3roSDYwi6<?62;)m6VVSJDYqKO~T%');
define('LOGGED_IN_SALT',   't$F_%R. }n<_,Itb+!|G@?0T-o:)V/aT^P2[`%ne)<p(67uDE{ed(=Imhg`#:Q4*');
define('NONCE_SALT',       'xQ?a4ll-{`q5|>ZTRj+*YWzWF8A6K7Yr@<Z;rAA_uEYM!5{Pur$o#Kq:XDOc9)EH');
define('FS_METHOD','direct');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
