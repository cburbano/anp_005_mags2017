<?php
    /**
    * Template Name: Jurados-diseño Page
    */
 
get_header(); ?>

<?php
get_sidebar();?>
<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php

				get_template_part( 'template-parts/content', 'jurados-diseno' );

				// If comments are open or we have at least one comment, load up the comment template.
				
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php 
get_footer();

 ?>


