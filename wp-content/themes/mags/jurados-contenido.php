<?php
    /**
    * Template Name: Jurados-contenido Page
    */
 
get_header(); ?>

<?php
get_sidebar();?>
<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php
				get_template_part( 'template-parts/content', 'jurados-contenido' );
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php 
get_footer();

 ?>


