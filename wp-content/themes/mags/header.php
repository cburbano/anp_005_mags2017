<?php
   /**
    * The header for our theme
    *
    * This is the template that displays all of the <head> section and everything up until <div id="content">
    *
    * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
    *
    * @package mags
    */
   
   ?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
   <head>
      <meta charset="<?php bloginfo( 'charset' ); ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="profile" href="http://gmpg.org/xfn/11">
      <?php wp_head(); ?>
   </head>
   <body <?php body_class(); ?>>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1118647864931360',
          xfbml      : true,
          version    : 'v2.8'
        });
        FB.AppEvents.logPageView();
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
       
      <div id="page" class="site">
      <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'mags' ); ?></a>
      <header>
         <div class="container center top">
            <h1 class="brand col-xs-12 col-sm-6 col-md-5">
               <a href="http://anp.cl">
               <img src="http://anp.cl/assets/img/header-h1.png" alt="ANP">
               </a>
            </h1>

            <form  style="display: none;" role="search" method="get" class="search-form" action="http://anp.cl/">
               <fieldset>
                  <input type="search" name="s" value="" class="search-desktop" placeholder="Buscar">
               </fieldset>
               <div class="input submit-mobile">  
                  <input type="submit" value="Buscar" id="submit-mobile">
               </div>
               <div class="input submit-desktop"> 
                  <input type="submit" value="Buscar" id="submit">
               </div>
            </form>

            <div class="socials">
               <p>Síguenos en</p>
               <a href="https://twitter.com/anpchile" class="tw" target="_blank"><span class="fa fa-twitter"></span></a>
               <a href="https://www.facebook.com/Asociaci%C3%B3n-Nacional-de-la-Prensa-1735555253364306/?fref=ts" class="fb" target="_blank"><span class="fa fa-facebook"></span></a>
            </div>
         </div>
      </header>
      <!-- #masthead -->
      <div class="wrap__mobile">
         <a href="#" id="open-nav"> <span class="fa fa-bars"></span></a>
      </div>
      <section class="pages home">
      <!--comienzo de seccion de contenido -->
      <div class="container">