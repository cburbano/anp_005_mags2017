<?php
    /**
    * Template Name: Participar Page
    */
 
get_header(); ?>

<?php
get_sidebar();?>
<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'como_participar' );

				// If comments are open or we have at least one comment, load up the comment template.
				

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php 
get_footer();

 ?>


