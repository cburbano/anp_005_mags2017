<?php
    /**
    * Template Name: Ganadores Page
    */
 
get_header(); ?>

<?php
get_sidebar();?>
<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'ganadores' );				

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php 
get_footer();

 ?>


