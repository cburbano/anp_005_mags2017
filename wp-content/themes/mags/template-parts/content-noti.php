<?php
   /**
    * Template part for displaying posts
    *
    * @link https://codex.wordpress.org/Template_Hierarchy
    *
    * @package mags
    */
   
   ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <header class="entry-header">
      <?php
         if ( is_single() ) :
            the_title( '<h1 class="entry-title">', '</h1>' );
         else :
            the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
         endif;
         
         if ( 'post' === get_post_type() ) : ?>
      <div class="entry-meta">
         <?php mags_posted_on(); ?>
      </div>
      <!-- .entry-meta -->
      <?php
         endif; ?>
   </header>
   <!-- .entry-header -->
   <div class="entry-content">
      <div class="col-xs-12 col-md-9 wrap__content">
         <div class="row">
            <div class="wrap__title-inner">
               <h1 class="title">Noticias</h1>
            </div>
            <div class="wrap__news ">
               <div class="row">
                  <?php $query = new WP_Query( 'cat=3' ); ?>
                  <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                  <div class="item__news news__jurado">
                     <div class="col-md-5 news__thumb">
                        <figure>
                           <?php echo the_post_thumbnail('medium'); ?>
                        </figure>
                     </div>

                     <div class="col-md-7 news__info">
                        <h2><?php the_title(); ?></h2>
                        <span><?php the_time('j F, Y'); ?></span>
                        <?php the_content(); ?>
                        

                        <div class="share__post">
                           <p>Compartir en:</p>
                            <a href="#" class="fb fbshare" data-title="ANP | Mags" data-message="<?php the_title(); ?>" data-picture="<?php echo the_post_thumbnail_url(); ?>" data-link="<?php bloginfo('url'); ?>"><span class="fa fa-facebook"></span></a>

                            <a href="#" class="tw twshare" data-message="<?php the_title(); ?>" data-link=""><span class="fa fa-twitter"></span></a>
                        </div>                           
                     </div>

                     <a href="#" class="open_description"><span class="fa fa-angle-down"></span></a>
                  </div>
                  <?php endwhile; else : ?>
                  <?php endif; ?>
               </div>
            </div>
            <div class="wrap__copy">
               <div class="copy__inner">
                  <a href="#">ACCESO JURADO</a>
                  <p>Copyright <span>MAGs</span>®  | Email: <a href="mailto:fbonati@anp.cl">fbonati@anp.cl</a></p>
               </div>
            </div>
         </div>
      </div>
   </div>
</article>
<!-- #post-## -->