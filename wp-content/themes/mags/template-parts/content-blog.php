<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package mags
 */

?>
<article d="post-<?php the_ID(); ?>" <?php post_class(); ?>>>
	<header class="entry-header">

			<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php mags_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php
			endif; ?>
	</header><!-- .entry-header -->
	<!-- .entry-header -->
	<div class="entry-content">
		<div class="col-xs-12 col-md-9 wrap__content">
			<div class="row">
				<div class="wrap__title-inner">
	               <h1 class="title"><?php echo "Ganadores "; the_title()?></h1>
	            </div>
	            <div class="wrap__news ganadores">
               <p class="title" style="margin-bottom: 20px !important;">El Premio Nacional de Revistas nace al interior de la Asociación Nacional de la Prensa en 2011, con el objetivo de reconocer y destacar la calidad del trabajo periodístico y gráfico realizado en la industria de las revistas chilenas. Pueden participar todas las revistas del país que cumplan con los requisitos establecidos.</p>

               <div class="wrap__winners">
                  <div class="items">	
					<?php 

					$images = get_field('gl_galeria');

					if( $images ): 
						foreach( $images as $image ): 
					?>
                     <div class="winner__item">
                        <a id="foto1" rel="gal" title="Titulo de la foto" class="various" href="<?php echo $image['url']; ?>">
                           <img src="<?php echo $image['sizes']['gallery-thumb']; ?>" alt="<?php echo $image['alt']; ?>" alt="">
                        </a>
                     </div> 
 					<?php endforeach; ?>
					<?php endif; ?>
                  </div>

                  <div class="wrap__download">
                     <a href="#">Descargar <span></span></a>
                  </div>

					<div class="share__post">
						<p>Compartir en:</p>
						<a href="#" class="fb fbshare" data-title="ANP | Mags" data-message="<?php echo "Ganadores "; the_title()?>" data-picture="<?php echo $image['url']; ?>" data-link="<?php bloginfo('url'); ?>"><span class="fa fa-facebook"></span></a>

						<a href="#" class="tw twshare" data-message="<?php echo "Ganadores "; the_title()?>" data-link=""><span class="fa fa-twitter"></span></a>
					</div>                  
               </div>

				<div class="wrap__player">
					<?php
					$iframe = get_field('gl_iframe');

					preg_match('/src="(.+?)"/', $iframe, $matches);
					$src = $matches[1];

					$params = array(
					    'controls'    => 0,
					    'hd'        => 1,
					    'autohide'    => 1
					);

					$new_src = add_query_arg($params, $src);

					$iframe = str_replace($src, $new_src, $iframe);

					$attributes = 'frameborder="0"';
					$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
					?>

					<div class="player">
						<?php echo $iframe; ?>
					</div>
				</div>               
            </div>

				<div class="contenido">
					<?php the_content(); ?>				
				</div>	
				 <div class="wrap__copy">
               <div class="copy__inner">
                  <a href="http://concurso.mags.cl/auth/login">ACCESO JURADO</a>
                  <p>Copyright <span>MAGs</span>®  | Email: <a href="mailto:fbonati@anp.cl">fbonati@anp.cl</a></p>
               </div>
            </div>			
			</div>

		</div>
	</div>
</article>

<!-- .entry-content -->
