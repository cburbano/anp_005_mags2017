<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mags
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">

		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php mags_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
	<div class="col-xs-12 col-md-9 wrap__content">
        <div class="row">
		     <div class="wrap__title-inner">
               <h1 class="title">Ganadores <span>/ 2016</span></h1>
            </div>

            <div class="wrap__news ganadores">
               <p class="title">El Premio Nacional de Revistas nace al interior de la Asociación Nacional de la Prensa en 2011, con el objetivo de reconocer y destacar la calidad del trabajo periodístico y gráfico realizado en la industria de las revistas chilenas. Pueden participar todas las revistas del país que cumplan con los requisitos establecidos.</p>

               <div class="wrap__winners">
                  <div class="items">
                     <div class="winner__item">
                        <a id="foto1" rel="gal" title="Titulo de la foto" class="various" href="http://mags.bitbanglab.cl/img/html/thumb-gallery.png">
                           <img src="<?php  echo get_template_directory_uri().'/assets/img/html/thumb-gallery.png'; ?>" alt="">
                           
                        </a>
                     </div>            

                     <div class="winner__item">
                        <a id="foto1" rel="gal" title="Titulo de la foto 2" class="various" href="http://mags.bitbanglab.cl/img/html/thumb-gallery.png">
                           <img src="<?php  echo get_template_directory_uri().'/assets/img/html/thumb-gallery.png'; ?>" alt="">
                        </a>
                     </div>                     
                  </div>

                  <div class="wrap__download">
                     <a href="#">Descargar <span></span></a>
                  </div>
               </div>
            </div>

            <div class="wrap__copy">
               <div class="copy__inner">
                  <a href="http://concurso.mags.cl/auth/login">ACCESO JURADO</a>
                  <p>Copyright <span>MAGs</span>®  | Email: <a href="mailto:fbonati@anp.cl">fbonati@anp.cl</a></p>
               </div>
            </div>
                 
            </div>
         </div>
	</div><!-- .entry-content -->

	<!--<footer class="entry-footer">
		<?php mags_entry_footer(); ?>
	</footer>--><!-- .entry-footer -->
</article><!-- #post-## -->
