<?php
   /**
    * Template part for displaying posts
    *
    * @link https://codex.wordpress.org/Template_Hierarchy
    *
    * @package mags
    */


                  
   ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <!-- .entry-header -->
   <div class="entry-content">
      <div class="col-xs-12 col-md-9 wrap__content">
         <div class="row">
            <div class="wrap__title-inner">
               <h1 class="title">Jurado / <span><?php the_title(); ?></span></h1>
            </div>
            <div class="wrap__news">
               <div class="row">                          
                  <?php 
                    $args = array('posts_per_page' => 2000, 'post_type' => 'jurado');
                    $query = new WP_Query( $args );
                    $cont = 0;
                  ?>          

                  <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                  <?php
                  $cont++;
                    global $post;
                      $categories = get_the_category($post->ID);
                      foreach($categories as $cd){
                        $cat = $cd->cat_name;
                      }
                      if($cat == 'digital'){
                      ?>
                  <div class="item__news news__jurado">
                     <div class="col-md-5 news__thumb">

                        <figure>
                           <?php echo the_post_thumbnail('news-thumb'); ?>
                        </figure>
                     </div>
                     <div class="col-md-7 news__info">
                        <h2><?php the_title(); ?></h2>
                        <span><?php echo get_field('j_cargo'); ?></span>
                        <div class="pre" id="pre_<?php echo $cont; ?>"><?php $content = get_the_content(); echo mb_strimwidth($content, 0, 135, '...');?></div>
                        <div class="all" id="all_<?php echo $cont; ?>"><?php the_content(); ?></div>

                     </div>
                     <a href="#" class="open_description" id="<?php echo $cont; ?>"><span class="fa fa-angle-down"></span></a>
                  </div>
                  <?php 
                    } ?>
                  <?php endwhile; else : ?>
                  <?php endif; ?>
               </div>
            </div>
            <div class="wrap__copy">
               <div class="copy__inner">
                  <a href="http://concurso.mags.cl/auth/login">ACCESO JURADO</a>
                  <p>Copyright <span>MAGs</span>®  | Email: <a href="mailto:fbonati@anp.cl">fbonati@anp.cl</a></p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- .entry-content -->
   <!--<footer class="entry-footer">
      <?php mags_entry_footer(); ?>
      </footer>--><!-- .entry-footer -->
</article>
<!-- #post-## -->