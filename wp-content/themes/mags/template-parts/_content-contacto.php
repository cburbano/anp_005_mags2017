<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mags
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php mags_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
	<div class="col-xs-12 col-md-9 wrap__content">
        <div class="row">
		      <div class="wrap__title-inner">
               <h1 class="title">Contacto</h1>
            </div>

            <div class="wrap__news contacto">
               <div class="row">
                  <div class="col-md-4 contacto__datos">
                     <h2>Datos</h2>

                     <p><span>Francesca Bonati</span></p>
                     <p><span>Fono:</span> 56 2 32133800</p>
                     <p><span>Email:</span> fbonati@anp.cl</p>
                     <p><span>Dirección:</span> Carlos Antúnez 2048, Providencia, Santiago</p>
                  </div>                 

                  <div class="col-md-8 contacto__form">
							<style>
								.contacto__form .form10  input[type="text"] {
									height: 44px !important;
									font-family: 'roboto';
									font-style: initial;
									font-size: 12px;
									padding: 0px 10px !important;	
								}	

								.contacto__form  .form10  textarea {
									font-family: 'roboto';
									font-size: 12px;
									font-style: initial;
									padding: 10px 10px !important;	
									resize: none;
								}
							</style> 

                     <h3>Si desea contactarse con los organizadores, ingrese sus datos y le responderemos a la brevedad.</h3>
                  	<?php wd_contact_form_maker(10); ?>
                  </div>                  
               </div>
            </div>

            <div class="wrap__copy">
               <div class="copy__inner">
                  <a href="http://concurso.mags.cl/auth/login">ACCESO JURADO</a>
                  <p>Copyright <span>MAGs</span>®  | Email: <a href="mailto:fbonati@anp.cl">fbonati@anp.cl</a></p>
               </div>
            </div>
                 
            </div>
         </div>
		<?php
			/*the_content( sprintf(
				/* translators: %s: Name of current post. 
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'mags' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'mags' ),
				'after'  => '</div>',
			) );*/
		?>
	</div><!-- .entry-content -->

	<!--<footer class="entry-footer">
		<?php mags_entry_footer(); ?>
	</footer>--><!-- .entry-footer -->
</article><!-- #post-## -->
