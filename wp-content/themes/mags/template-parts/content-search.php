<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mags
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-summary">
		<div class="col-md-7">
			<?php the_excerpt(); ?>
		</div>		
	</div><!-- .entry-summary -->
</article><!-- #post-## -->
