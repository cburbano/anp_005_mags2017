<?php
   /**
    * Template part for displaying posts
    *
    * @link https://codex.wordpress.org/Template_Hierarchy
    *
    * @package mags
    */
   
   ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <header class="entry-header">
      <?php
         if ( is_single() ) :
            the_title( '<h1 class="entry-title">', '</h1>' );
         else :
            the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
         endif;
         
         if ( 'post' === get_post_type() ) : ?>
      <div class="entry-meta">
         <?php mags_posted_on(); ?>
      </div>
      <!-- .entry-meta -->
      <?php
         endif; ?>
   </header>
   <!-- .entry-header -->
   <div class="entry-content">
      <div class="col-xs-12 col-md-9 wrap__content">
         <div class="row">
          <div class="wrap__title-inner">
               <h1 class="title">¿Cómo participar?</h1>
            </div>

            <div class="wrap__news como__participar">
               <h2>Pasos para postular a los premios MAGs 2017:</h2>

               <div class="wrap_steps">
                  <div class="step">
                     <figure>
                        <img src="<?php  echo get_template_directory_uri().'/assets/img/svg/usuarios.svg' ?>" alt="Mags">
                     </figure>
                     <p>Cada revista o suplemento deberá designar a una persona la cual deberá ingresar a www.mags.cl y completar los datos que están en el botón <strong>POSTULA AQUÍ</strong>.</p>
                  </div>

                  <div class="step">
                     <figure>
                        <img src="<?php  echo get_template_directory_uri().'/assets/img/svg/mail.svg' ?>" alt="Mags">
                     </figure>
                     <p>Luego le llegará una clave al mail inscrito (revisar spam o correo no deseado en caso de no recibirlo en la bandeja de entrada), con la cual podrá ingresar.</p>
                  </div>

                  <div class="step">
                     <figure>
                        <img src="<?php  echo get_template_directory_uri().'/assets/img/svg/form.svg' ?>" alt="Mags">
                     </figure>
                     <p>Al ingresar deberá completar los datos que se solicitan, luego elegir a qué área de postulación pertenece la revista o suplemento y posteriormente seleccionar el premio al cual quieren postular y subir el PDF.</p>
                  </div>

                  <div class="step">
                     <figure>
                        <img src="<?php  echo get_template_directory_uri().'/assets/img/svg/paper.svg' ?>" alt="Mags">
                     </figure>
                     <p>En paralelo a la postulación online, deberán enviar una copia de cada ejemplar donde se encuentra la pieza postulada a la dirección Carlos Antúnez 2048, Providencia a nombre de Francesca Bonati.</p>
                  </div>

                  <div class="step">
                     <figure>
                        <img src="<?php  echo get_template_directory_uri().'/assets/img/svg/card.svg' ?>" alt="Mags">
                     </figure>
                     <p>Para finalizar y poder participar en la evaluación, los medio deberán realizar el pago equivalente al número de postulaciones realizadas. (Detalle en las bases)</p>
                  </div>                                                                        
               </div>

               <h3>Para cualquier duda en el proceso, enviar un mail a <a class="email_contact" href="mailto:fbonati@anp.cl">fbonati@anp.cl</a></h3>

               <div class="wrap__download">
                  <a href="#">Bases MAGs 2017 <span></span></a>
                  <a href="#">Definición de premios 2017 <span></span></a>
               </div>
            </div>

            <div class="wrap__copy">
               <div class="copy__inner">
                  <a href="http://concurso.mags.cl/auth/login">ACCESO JURADO</a>
                  <p>Copyright <span>MAGs</span>®  | Email: <a href="mailto:fbonati@anp.cl">fbonati@anp.cl</a></p>
               </div>
            </div>
         </div>
      </div>
   </div>
</article>
<!-- #post-## -->