<?php
   /**
    * Template part for displaying posts
    *
    * @link https://codex.wordpress.org/Template_Hierarchy
    *
    * @package mags
    */

    $args = array( 'post_type' => 'presidentes');
    $query = new WP_Query( $args );


                  
   ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <!-- .entry-header -->
   <div class="entry-content">
      <div class="col-xs-12 col-md-9 wrap__content">
         <div class="row">
            <div class="wrap__title-inner">
               <h1 class="title">Jurado / <span><?php the_title(); ?></span></h1>
            </div>
            <div class="wrap__news">
               <div class="row">                          
                  <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                  <div class="item__news presidente">
                     <div class="col-md-12 news__thumb">
                        <figure>
                           <?php echo the_post_thumbnail(''); ?>
                        </figure>
                     </div>
                     <div class="col-md-12 news__info">
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                     </div>
                  </div>
                  <?php endwhile; else : ?>
                  <?php endif; ?>
               </div>
            </div>
            <div class="wrap__copy">
               <div class="copy__inner">
                  <a href="http://concurso.mags.cl/auth/login">ACCESO JURADO</a>
                  <p>Copyright <span>MAGs</span>®  | Email: <a href="mailto:fbonati@anp.cl">fbonati@anp.cl</a></p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- .entry-content -->
   <!--<footer class="entry-footer">
      <?php mags_entry_footer(); ?>
      </footer>--><!-- .entry-footer -->
</article>
<!-- #post-## -->