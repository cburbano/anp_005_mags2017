<?php
/**
 * mags functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package mags
 */

if ( ! function_exists( 'mags_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function mags_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on mags, use a find and replace
	 * to change 'mags' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'mags', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'mags' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'mags_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'mags_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function mags_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'mags_content_width', 640 );
}
add_action( 'after_setup_theme', 'mags_content_width', 0 );

add_action('init', 'my_custom_init');

function my_custom_init() {
    // remember add first parameter post type as 'page'
    add_post_type_support( 'page', 'page-attributes' );
}
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mags_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'mags' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'mags' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'MenuSidebar', 'mags' ),
		'id'            => 'menu_sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'mags' ),
		'before_widget' => '<div class="wrap__aside-fix">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'mags_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function mags_scripts() {
	wp_enqueue_style( 'mags-style', get_stylesheet_uri() );
	wp_enqueue_style( 'main', get_template_directory_uri(). '/assets/css/main.css' );
	wp_enqueue_style( 'news', get_template_directory_uri(). '/assets/css/news.css' );
	wp_enqueue_style( 'roboto', '//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' );
	wp_enqueue_style( 'fancy', get_template_directory_uri(). '/assets/libs/fancybox/source/jquery.fancybox.css' );
	wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
	wp_enqueue_style( 'bootstrap_font', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
	wp_enqueue_style( 'OwlCarouseCss', get_template_directory_uri().'/assets/libs/OwlCarousel2/dist/assets/owl.carousel.css' );

	wp_enqueue_style( 'OwlCarouse-minCss', get_template_directory_uri().'/assets/libs/OwlCarousel2/dist/assets/owl.carousel.min.css' );

	wp_enqueue_script( 'jquery_lib', get_template_directory_uri() . '/assets/libs/jquery/dist/jquery.min.js', array(), '20151215', true );

	wp_enqueue_script( 'mags-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'fancyboxJS', get_template_directory_uri() . '/assets/libs/fancybox/source/jquery.fancybox.js', array(), '20151215', true );

	wp_enqueue_script( 'OwlCarouse-min', get_template_directory_uri() . '/assets/libs/OwlCarousel2/dist/owl.carousel.min.js', array(), '', true );

	wp_enqueue_script( 'OwlCarouse', get_template_directory_uri() . '/assets/libs/OwlCarousel2/dist/owl.carousel.js', array(), '', true );

	wp_enqueue_script( 'slider', get_template_directory_uri() . '/assets/js/slider-home.js', array(), '', true );

	wp_enqueue_script( 'mags-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array(), '20151215', true );

	wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDpRo2qq0OalhNaomCf8CvWNDETUxfmZPo', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'mags_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Tipos de contenido
 */

function custom_post_type() {	
	$slide = array(
		'label'               => __( 'Sliders' ),
		'description'         => __( 'Sliders' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		
		// This is where we add taxonomies to our CPT
		'taxonomies'          => array( 'category' ),
	);

	$presidentes = array(
		'label'               => __( 'Presidentes' ),
		'description'         => __( 'Presidentes' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		
		// This is where we add taxonomies to our CPT
		'taxonomies'          => array( 'category' ),
	);	

	$jurado = array(
		'label'               => __( 'Jurado' ),
		'description'         => __( 'Jurado' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		
		// This is where we add taxonomies to our CPT
		'taxonomies'          => array( 'category' ),
	);	

	$ganadores = array(
		'label'               => __( 'Ganadores' ),
		'description'         => __( 'Ganadores' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		
		// This is where we add taxonomies to our CPT
		'taxonomies'          => array( 'category' ),
	);	
	$galeria = array(
		'label'               => __( 'Galeria' ),
		'description'         => __( 'Galeria' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		
		// This is where we add taxonomies to our CPT
		'taxonomies'          => array( 'category' ),
	);			

	// Registering your Custom Post Type
	register_post_type( 'sliders', $slide );
	register_post_type( 'presidentes', $presidentes );
	register_post_type( 'jurado', $jurado );
	register_post_type( 'ganadores', $ganadores );
	register_post_type( 'galeria', $galeria );
}

add_action( 'init', 'custom_post_type', 0 );



/**
 * Tamaño de imagenes
 */
add_image_size( 'gallery-thumb', 390, 229, true );
add_image_size( 'news-thumb', 290, 170, true );

/**
 * menu
 */

function register_my_menus() {
  register_nav_menus(
    array(
      'principal' => __( 'Principal' ),
      'extra-menu' => __( 'Extra Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

/**
 * Opciones Globales
 */

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();	
}