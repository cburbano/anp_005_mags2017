<?php
   /**
    * The sidebar containing the main widget area
    *
    * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
    *
    * @package mags
    */

   if ( ! is_active_sidebar( 'sidebar-1' ) ) {
   	return;
   }
?>
<aside id="secondary" class="col-xs-12 col-md-3 aside" role="complementary">
   <div class="row">
      <div class="wrap__aside-fix">
         <div class="aside__brand">
            <a href="<?php bloginfo(url) ?>"><img src="<?php echo get_template_directory_uri().'/assets/img/html/sidebar-mags-logo.png'; ?>" alt="Mags"></a>
         </div>

         <div class="aside__postula">
            <a href="http://concurso.mags.cl/auth/login">Postula Aquí</a>
         </div>

         <div class="aside__login">
            <a href="http://concurso.mags.cl/">Login</a>
         </div>

          <?php wp_nav_menu( array( 'theme_location' => 'principal' ) ); ?>

         <div class="aside__bases">
            <a href="<?php echo get_field('options_bases', 'option'); ?>" target="_blank">DESCARGAR BASES</a>
         </div>

         <p>Copyright <span>MAGs</span>®</p>
      </div>
      <?php //dynamic_sidebar( 'sidebar-1' ); ?>
   </div>
</aside>
<!-- #secondary
