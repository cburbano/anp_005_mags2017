$(function(){
    $('#sliderHome').owlCarousel({
		loop:true,
		margin:0,
		nav:false,
		items: 1,
		autoplay:false,
		autoplayTimeout:5000,
		autoplayHoverPause:true        
    });

});    