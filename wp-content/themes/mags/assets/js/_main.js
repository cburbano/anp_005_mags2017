$(function(){
	$('#open-nav').click(function(e) {
		$('.aside').toggleClass('enabled');

		e.preventDefault();
	});

	$('.open_description').click(function(e){
		$(this).parent().toggleClass('open_description');

		e.preventDefault();
	});

	$('.menu-item-has-children > a').click(function(e){
		$(this).parent().toggleClass('open');

		e.preventDefault();
		e.stopPropagation();
	});

	$(".various").fancybox({
		maxWidth		: 800,
		maxHeight	: 600,
		width			: '70%',
		height		: '70%',
		loop 			: false,
		beforeLoad: function() {
			// var title = $(this.title);

			// setTimeout(function(){
			// 	var img = $('.fancybox-image').attr('src');
			// 	var URLactual = window.location;

			// 	$('.fancybox-skin').append(
			// 		'<div class="share__post--lightbox">'+
			// 		   '<p>Compartir en:</p>'+
			// 		   '<a href="#" id="fbShare" class="fb" data-title="Titulo de facebook" data-message=" '+ title.selector +' " data-picture="'+ img +' " data-link="' + URLactual + '"><span class="fa fa-facebook"></span></a>'+
			// 		   '<a href="#" id="twShare" class="tw" data-message=" '+ title.selector +' " data-image=" '+ img +' " data-link="' + URLactual + '"><span class="fa fa-twitter"></span></a>'+
			// 		'</div>'
			// 	);		

			// 	$('#fbShare').click(function (e) {
			// 	   var data = $(this).data();

			// 	   if (typeof FB !== 'undefined') {
			// 	     FB.ui({
			// 	       method: 'feed',
			// 	       name: data.title,
			// 	       link: data.link,
			// 	       picture: data.picture,
			// 	       description: data.message
			// 	     });
			// 	   }

			// 	   e.preventDefault();
			// 	});

			// 	$('#twShare').click(function (e) {
			// 	 var data = $(this).data(),
			// 	   message = encodeURIComponent(data.message),
			// 	   link = encodeURIComponent(data.link);

			// 	 window.open('https://twitter.com/share?text=' + message + '&url=' + link, 'sharer', 'toolbar=0, status=0, width=626, height=436');

			// 	 e.preventDefault();
			// 	});							
			// },100);		
		},	
		afterShow: function(){          
		}				
	});


	$('.open__dropdown').click(function(e){
		$(this).parent().toggleClass('open');
		//$(this).css('text-decoration', 'none');
		e.preventDefault();
	});

	var thisHash = window.location.hash;

	if(window.location.hash) {
		$(thisHash).fancybox({
		 padding: 0
		}).trigger('click');
	}

	$(document).ready(function(e){
		var href = window.location.href;
		var a = $('a[href="'+href+'"]');

		if(a.parent().is('.jurado')){
			$('.sub_a').toggleClass('open');
			a.addClass('menu_li_sub');
		}
		if(a.parent().is('.ganadores')){
			$('.sub_b').toggleClass('open');
			a.addClass('menu_li_sub');
		}

		if( !a.is( ".jurado" ) && !a.is( ".ganadores" )){
			a.addClass('menu_li');
		}
	});

	$('.various').fancybox({
		padding: 0
	});	
	
	$('.fbshare').click(function (e) {
	   var data = $(this).data();

	   if (typeof FB !== 'undefined') {
	     FB.ui({
	       method: 'feed',
	       name: data.title,
	       link: data.link,
	       picture: data.picture,
	       description: data.message
	     });
	   }

	   e.preventDefault();
	});

	$('.twshare').click(function (e) {
	 var data = $(this).data(),
	   message = encodeURIComponent(data.message),
	   link = encodeURIComponent(data.link);

	 window.open('https://twitter.com/share?text=' + message + '&url=' + link, 'sharer', 'toolbar=0, status=0, width=626, height=436');

	 e.preventDefault();
	});		
});