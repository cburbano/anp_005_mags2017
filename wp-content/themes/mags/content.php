<?php
   /**
    * Template part for displaying posts
    *
    * @link https://codex.wordpress.org/Template_Hierarchy
    *
    * @package mags
    */
   
   ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <!-- .entry-header -->
   <div class="entry-content">
      <div class="col-xs-12 col-md-9 wrap__content">
         <div class="row">
         <!-- slider-home-->
            <div class="wrap__slider">
              <?php 
                  $args = array( 'post_type' => 'sliders');
                  $query = new WP_Query( $args );

                  $slTitle = get_field('sl_title');
                  $slDesc = get_field('sl_descripcion');
              ?>             
              <div id="sliderHome" class="content__slider owl-carousel owl-theme">
                  <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                      <div class="slide">
                          <?php echo the_post_thumbnail(''); ?> 

                          <div class="slide__info">
                              <h1><?php the_title(); ?></h1>

                              <?php the_content(); ?>
                          </div>
                      </div>        
                  <?php endwhile; else : ?>
                  <?php endif; ?>
              </div>            
               <div class="content__participa">
                  <a href="#">HAZ CLIC AQUÍ <br/> Y DESCUBRE CÓMO PARTICIPAR</a>
               </div>
            </div>
         <!-- slider-home-->

         <!-- noticias -->
            <div class="wrap__news">
               <h1 class="title">Noticias</h1>
               <div class="row">    
               
               <?php $query = new WP_Query( 'cat=3' ); ?>
 				       <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                  <div class="item__news news__jurado">
                     <div class="col-md-5 news__thumb">
                        <figure>
                            <?php echo the_post_thumbnail('news-thumb'); ?>
                        </figure>
                     </div>
                     <div class="col-md-7 news__info">
                        <h2><?php the_title(); ?></h2>

                        <span><?php the_time('j F, Y'); ?></span>

                        <div class="wrap__text">
                          <?php the_content(); ?>
                        </div>
                     </div>
                     <a href="#" class="open_description"><span class="fa fa-angle-down"></span></a>
                  </div>

               <?php endwhile; else : ?>

               <?php endif; ?>
 
               </div>
            </div>

          <!-- noticias -->
          <!--Galeria -->
            <div class="wrap__gallery">
               <h1 class="title">Galería de ganadores</h1>
               <?php 
                  $args = array( 'post_type' => 'galeria');
                  $query = new WP_Query( $args );
                ?>  
                
               <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                  <div class="gallery__item">
                    <a href="<?php the_permalink(); ?>">
                      <span><?php the_title(); ?></span>

                      <figure>
                         <?php echo the_post_thumbnail(''); ?>
                      </figure>
                    </a>
                  </div>
                  <?php endwhile; else : ?>

               <?php endif; ?>
               
               </div>
               <div class="wrap__sponsor">
                  <div class="sponsor__item">
                     <h3>Organiza:</h3>
                     <div class="thumb">
                        <img src="<?php  echo get_template_directory_uri().'/assets/img/html/sponsor-logo-anp.png' ?>" alt="ANP">
                     </div>
                  </div>
                  <div class="sponsor__item">
                     <h3>Auspicia:</h3>
                     <div class="thumb">
                        <img src="<?php  echo get_template_directory_uri().'/assets/img/html/sponsor-logo-amf.png' ?>" alt="ANP">
                     </div>
                  </div>
                  <div class="sponsor__item">
                     <h3>Colabora:</h3>
                     <div class="thumb">
                        <img src="<?php  echo get_template_directory_uri().'/assets/img/html/sponsor-logo-marriot.png' ?>" alt="ANP">
                     </div>
                  </div>
               </div>

                  <div class="wrap__copy">
                  <div class="copy__inner">
                     <a href="#">ACCESO JURADO</a>
                     <p>Copyright <span>MAGs</span>®  | Email: <a href="mailto:fbonati@anp.cl">fbonati@anp.cl</a></p>
                  </div>
            </div>
          <!-- / Galeria -->
         </div>
      </div>
   </div>
   <!-- .entry-content -->
</article>
<!-- #post-## -->