<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mags
 */

?>

	 </div>
  </section> <!-- fin de seccion de contenido -->

	<footer>
		<div class="container center inner">
			<div class="row">
	         <div class="brand col-xs-12 col-sm-12 col-md-2">
	            <figure>
	               <a href="http://anp.cl">
	               <img src="http://anp.cl/assets/img/footer-brand.png" alt="ANP">
	               </a>
	            </figure>
	         </div>
	         <div class="mail col-xs-12 col-sm-12 col-md-2">
	            <span class="fa fa-envelope-o"></span>
	            <p><a href="mailto:info@anp.cl">info@anp.cl</a></p>
	         </div>
	         <div class="phone col-xs-12 col-sm-12 col-md-2">
	            <span class="fa fa-mobile"></span>
	            <p><a href="tel:+562 3213 3800">+562 3213 3800</a></p>
	         </div>
	         <div class="address col-xs-12 col-sm-12 col-md-6">
	            <span class="fa fa-map-marker"></span>
	            <p><a href="https://goo.gl/maps/Ey1tN" target="_blank">Av. Carlos Antúnez 2048, Providencia, Santiago, Chile</a></p>
	         </div>
	      </div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
	<div class="copyright">
	   <div class="container center inner">
	      <p>Copyright ® Asociación Nacional de la Prensa</p>
	      <a href="http://bitbang.cl/" target="_blank" class="brand"><img src="http://anp.cl/assets/img/bitbang-logo.png" alt="Bitbang Estudio"></a>
	   </div>
	</div>	
</div><!-- #page -->


<?php wp_footer(); ?>
</body>
</html>
